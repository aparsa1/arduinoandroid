package com.luc.bluetooth;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Control extends Activity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mOrientation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
        // You must implement this callback in your code.
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void move(View view) {

        int id=view.getId();

        switch (id) {
            case R.id.btnForward:
                Toast.makeText(this,"Moving Forward",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnLeft:
                Toast.makeText(this,"Turning Left",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnRight:
                Toast.makeText(this,"Turning Right",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnReverse:
                Toast.makeText(this,"Moving Backward",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnHorn:
                Toast.makeText(this,"Horn",Toast.LENGTH_SHORT).show();
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

//        findViewById(R.id.btnForward).setBackgroundColor(Color.BLUE);
    }



    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float azimuth_angle = event.values[0];
        float pitch_angle = event.values[1];
        float roll_angle = event.values[2];

        Button btnLeft=(Button)findViewById(R.id.btnLeft);
        Button btnRight=(Button)findViewById(R.id.btnRight);
        if(roll_angle>20)
        {
           btnLeft.setBackgroundColor(Color.YELLOW);
            btnRight.setBackgroundColor(Color.BLUE);
           //Toast.makeText(this,"Turn Left",Toast.LENGTH_SHORT).show();
        }
        else if (roll_angle<-20)
        {
            btnRight.setBackgroundColor(Color.YELLOW);
            btnLeft.setBackgroundColor(Color.BLUE);
           // Toast.makeText(this,"Turn Right",Toast.LENGTH_SHORT).show();
        }
        else
        {
            btnRight.setBackgroundColor(Color.BLUE);
            btnLeft.setBackgroundColor(Color.BLUE);
        }

    }
}
